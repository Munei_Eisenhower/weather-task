package za.co.mmbadimunei.weatherforecast;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RadioGroup;

public class Setting extends AppCompatActivity {
    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    RadioGroup radioGroupType;
    String currentUnit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        currentUnit = sharedpreferences .getString("unitType", "metric");
        radioGroupType =(RadioGroup)findViewById(R.id.radioType);
        if(currentUnit.equals("metric")){
            radioGroupType.check(R.id.radioButtonC);
        }else{
            radioGroupType.check(R.id.radioButtonF);
        }


        radioGroupType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(radioGroup.getCheckedRadioButtonId()==R.id.radioButtonC){
                    currentUnit = "metric";
                }else if(radioGroup.getCheckedRadioButtonId()==R.id.radioButtonF){
                    currentUnit = "imperial";
                }
                editor.putString("unitType",currentUnit);
                editor.commit();
            }
        });


    }
}
