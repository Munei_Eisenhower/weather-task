package za.co.mmbadimunei.weatherforecast;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;
import com.jetradarmobile.rxlocationsettings.RxLocationSettings;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import rx.functions.Action1;
import za.co.mmbadimunei.weatherforecast.Connection.JSONParser;
import za.co.mmbadimunei.weatherforecast.Connection.MyNetwork;


public class MainActivity extends AppCompatActivity {

    TextView tvLocation, tvDate, tvWeatherCondition, tvWeatherConditionDes, tvTemp, tvUnit, tvLastUpdate,tvSunset,tvSunrise;
    ImageView imageViewIcon;
    private String UNIT_TYPE = "metric";
    private String UNIT_SYMBOL = "C";
    private Location location;


    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    GPSTracker gpsTracker ;
    public ProgressDialog mProgressDialog;

    DisplayImageOptions options;
    ImageLoader imageLoader;

    Snackbar snackbar=null;
    private boolean isReceiverRegistered = false;
    NetworkChangeReceiver networkChangeReceiver;

    RelativeLayout relativeLayoutRoot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       if( getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_PORTRAIT){
           setPortrait();
       }else{
           setLandscape();
       }

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

       UNIT_TYPE= sharedpreferences .getString("unitType", UNIT_TYPE);
        if(UNIT_TYPE.equals("metric")){
            UNIT_SYMBOL="C";
        }else{
            UNIT_SYMBOL="F";
        }


        relativeLayoutRoot =(RelativeLayout)findViewById(R.id.root_layer);
        tvLocation = (TextView) findViewById(R.id.textViewLocation);
        tvDate = (TextView) findViewById(R.id.textViewDate);
        tvWeatherCondition = (TextView) findViewById(R.id.textViewCondition);
        tvWeatherConditionDes = (TextView) findViewById(R.id.textViewConditionDes);
        tvTemp = (TextView) findViewById(R.id.textViewTemp);
        tvUnit = (TextView) findViewById(R.id.textViewTempType);
        tvLastUpdate = (TextView) findViewById(R.id.textViewLastUpdate);
        tvSunrise = (TextView) findViewById(R.id.textViewSunrise);
        tvSunset = (TextView) findViewById(R.id.textViewSunset);
        imageViewIcon = (ImageView) findViewById(R.id.imageViewIcon);


        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(R.string.loading));
        mProgressDialog.setIndeterminate(true);

        networkChangeReceiver = new NetworkChangeReceiver();
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.loading)
                .showImageForEmptyUri(R.drawable.img_not_available)
                .showImageOnFail(R.drawable.img_not_available)
                .delayBeforeLoading(1000)
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        imageLoader = ImageLoader.getInstance();
        if(!ImageLoader.getInstance().isInited()) {
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                    .build();
            ImageLoader.getInstance().init(config);
        }

        gpsTracker = new GPSTracker(this);
        if(!gpsTracker.canGetLocation) {
            ensureLocationSettings();
        }
        ShowSneakBar(MyNetwork.isNetworkAvailable(this));






    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(MainActivity.this, Setting.class));
            return true;
        } else if (id == R.id.action_refresh) {
            ShowSneakBar(MyNetwork.isNetworkAvailable(this));
        }

        return super.onOptionsItemSelected(item);
    }

    public double convertTemp(double degree) {


        return 5;
    }

    private void ensureLocationSettings() {
        final LocationSettingsRequest locationSettingsRequest = new LocationSettingsRequest.Builder()
                .addLocationRequest(LocationRequest.create().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY))
                .build();
        RxLocationSettings.with(this).ensure(locationSettingsRequest)
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean enabled) {

                        Toast.makeText(MainActivity.this, enabled ? "Enabled" : "Failed", Toast.LENGTH_LONG).show();
                    }
                });
    }
    private class GetWeather extends AsyncTask<String,Void,JSONObject> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONParser jsonParser = new JSONParser();
            return jsonParser.getJSONFromUrl(params[0]);
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);

            if(jsonObject!=null) {


                Log.i("restults", jsonObject.toString());
                try{


                    relativeLayoutRoot.setVisibility(View.VISIBLE);

                    tvLocation.setText(jsonObject.optString("name")+"  "+jsonObject.optJSONObject("sys").optString("country"));
                    tvTemp.setText(jsonObject.optJSONObject("main").optString("temp")+"\u00B0");
                    tvUnit.setText(UNIT_SYMBOL);
                    tvWeatherCondition.setText(jsonObject.optJSONArray("weather").optJSONObject(0).optString("main"));
                    tvWeatherConditionDes.setText(jsonObject.optJSONArray("weather").optJSONObject(0).optString("description"));
                    tvWeatherConditionDes.setText(jsonObject.optJSONArray("weather").optJSONObject(0).optString("description"));
                    tvDate.setText(convertDate(String.valueOf(System.currentTimeMillis()),"E,dd MMM "));
                    tvSunrise.setText(convertDate(String.valueOf(jsonObject.optJSONObject("sys").optLong("sunrise")*1000),"kk:mm"));
                    tvSunset.setText(convertDate(String.valueOf(jsonObject.optJSONObject("sys").optLong("sunset")*1000),"kk:mm"));
                    String img = getString(R.string.server_img_url)+jsonObject.optJSONArray("weather").optJSONObject(0).optString("icon")+".png";
                    tvLastUpdate.setText(convertDate(String.valueOf(jsonObject.optLong("dt")*1000),"dd-MM-yyyy kk:mm"));

                    imageLoader.displayImage(img,imageViewIcon,options,null);

                }catch (Exception e){
                    e.printStackTrace();
                }

                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();

                }

            }

        }
    }
    public static String convertDate(String dateInMilliseconds,String dateFormat) {
        return DateFormat.format(dateFormat, Long.parseLong(dateInMilliseconds)).toString();
    }
    public void ShowSneakBar(boolean isConnected){
//if it is connected to the network close snackbar
        if(isConnected) {
            if(snackbar!=null) {
                snackbar.dismiss();
            }

          location = gpsTracker.getLocation();
            if(location!=null){
                double lat = location.getLatitude();
                double lgn = location.getLongitude();
                new GetWeather().execute(getString(R.string.server_url)+"lat="+lat+"&lon="+lgn+"&units="+UNIT_TYPE+"&APPID="+getString(R.string.server_api_key));
            }

        }else {
            //if it is not connected and was busy loading dismiss ProgressDialog
            if (mProgressDialog != null) {
                mProgressDialog.dismiss();
            }

//Write a red message on snackbar to show not connected
            String message;
            int color;

            message = "Sorry! Not connected to internet";
            color = Color.RED;
            View parentLayout = findViewById(R.id.root_layer);

            snackbar = Snackbar
                    .make(parentLayout, message, Snackbar.LENGTH_INDEFINITE);

            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }
    }

    //Simple Network BroadcastReceiver to check changes on network state

    public class NetworkChangeReceiver extends BroadcastReceiver {
        public NetworkChangeReceiver() {
        }

        @Override
        public void onReceive(final Context context, final Intent intent) {

            boolean status = MyNetwork.isNetworkAvailable(context);

//if any change call this method
            ShowSneakBar(status);


        }

    }
    protected void onResume() {
        super.onResume();
        //register BroadcastReceiver listener
        if (!isReceiverRegistered) {
            isReceiverRegistered = true;

            registerReceiver(networkChangeReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")); // IntentFilter to wifi state change is "android.net.conn.CONNECTIVITY_CHANGE"
        }
    }
    protected void onPause() {
        super.onPause();
        //unregister  BroadcastReceiver listener
        if (isReceiverRegistered) {
            isReceiverRegistered = false;

            unregisterReceiver( networkChangeReceiver);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setLandscape();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            setPortrait();
        }

    }

    public void setPortrait(){
        ((LinearLayout)findViewById(R.id.topContainer)).setOrientation(LinearLayout.VERTICAL);
        ((LinearLayout)findViewById(R.id.topContainer2)).setOrientation(LinearLayout.VERTICAL);
        ((LinearLayout)findViewById(R.id.mainContainer)).setOrientation(LinearLayout.VERTICAL);

    }
    public  void setLandscape(){
        ((LinearLayout)findViewById(R.id.topContainer)).setOrientation(LinearLayout.HORIZONTAL);
        ((LinearLayout)findViewById(R.id.topContainer2)).setOrientation(LinearLayout.HORIZONTAL);
        ((LinearLayout)findViewById(R.id.mainContainer)).setOrientation(LinearLayout.HORIZONTAL);

    }

}
